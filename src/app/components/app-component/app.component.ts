import {Component, Type, ViewChild, ViewContainerRef} from '@angular/core';
import {DomUtilsService} from '../../services/dom-utils.service';
import {HttpService} from '../../services/http.service';
import {WidgetModel} from '../../models/WidgetModel';
import {WidgetBase} from '../widgets/widget-base';
import {DevicesWidgetComponent} from '../widgets/devices-widget/devices-widget.component';
import {CountriesWidgetComponent} from '../widgets/countries-widget/countries-widget.component';
import {DemographicsWidgetComponent} from '../widgets/demographics-widget/demographics-widget.component';
import {AuctionsWidgetComponent} from '../widgets/auctions-widget/auctions-widget.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [
    DomUtilsService,
    HttpService
  ]
})
export class AppComponent {

  private static WIDGET_TYPES: Map<string, Type<WidgetBase>> = new Map([
    ['auctions', AuctionsWidgetComponent],
    ['devices', DevicesWidgetComponent],
    ['demographics', DemographicsWidgetComponent],
    ['countries', CountriesWidgetComponent]
  ]);

  /**
   * Array of chart's types for DropDown
   * @type {WidgetModel[]}
   */
  widgets = [
    new WidgetModel(null, 'none', null)
  ];

  /**
   *
   * @type {WidgetModel}
   */
  widgetSelected = this.widgets[0];

  /**
   *
   */
  @ViewChild('widgetsContainer', {read: ViewContainerRef})
  container: ViewContainerRef;

  /**
   *
   * @param {DomUtilsService} domUtilsService
   * @param {HttpService} httpService
   */
  constructor(private domUtilsService: DomUtilsService, private httpService: HttpService) {
    httpService.loadData().subscribe(
      d => this.parseData(d),
      console.log
    );
  }

  /**
   * Click handler
   */
  onAddClick() {
    if (this.widgetSelected.component) {
      const widget = this.domUtilsService.addComponent(this.container, this.widgetSelected.component);

      widget.instance.init(this.widgetSelected.title, this.widgetSelected.data);
      widget.instance.remove.subscribe(() => widget.destroy());
      widget.onDestroy(() => widget.instance.remove.unsubscribe());
    }
  }

  /**
   * Filling Array of WidgetModels from RAW JSON data
   * @param {JSON} data
   */
  private parseData(data: JSON) {
    for (const key of Object.keys(data)) {
      const type = AppComponent.WIDGET_TYPES.get(key);
      if (type) {
        this.widgets.push(new WidgetModel(type, key, data[key]));
      }
    }
  }
}
