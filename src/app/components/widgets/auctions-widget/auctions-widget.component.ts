import {Component} from '@angular/core';
import {WidgetBase} from '../widget-base';

@Component({
  selector: 'app-auctions-widget',
  templateUrl: './auctions-widget.component.html',
  styleUrls: ['./auctions-widget.component.less']
})
export class AuctionsWidgetComponent extends WidgetBase {

  /**
   *
   */
  constructor() {
    super();
  }

  /**
   *
   * @param {string} title
   * @param {JSON} data
   */
  init(title: string, data: JSON) {
    super.init(title, data);

    this.chartType = 'line';
    this.chartLabels = data['chartLabels'];
    this.chartData = data['chartData'];
    this.chartOptions = {
      responsive: true,
      elements: {
        line: {
          tension: 0,
          fill: false
        }
      }
    };
  }

}
