import {Component} from '@angular/core';
import {WidgetBase} from '../widget-base';

@Component({
  selector: 'app-devices-widget',
  templateUrl: './devices-widget.component.html',
  styleUrls: ['./devices-widget.component.less']
})
export class DevicesWidgetComponent extends WidgetBase {

  /**
   *
   */
  constructor() {
    super();
  }

  /**
   *
   * @param {string} title
   * @param {JSON} data
   */
  init(title: string, data: JSON) {
    super.init(title, data);

    this.chartType = 'doughnut';
    this.chartLabels = data['chartLabels'];
    this.chartData = data['chartData'];
    this.chartOptions = {
      responsive: true,
      cutoutPercentage: 70,
      legend: {
        position: 'right'
      }
    };
  }

}
