import {Component} from '@angular/core';
import {WidgetBase} from '../widget-base';

@Component({
  selector: 'app-demographics-widget',
  templateUrl: './demographics-widget.component.html',
  styleUrls: ['./demographics-widget.component.less']
})
export class DemographicsWidgetComponent extends WidgetBase {

  /**
   *
   */
  constructor() {
    super();
  }

  /**
   *
   * @param {string} title
   * @param {JSON} data
   */
  init(title: string, data: JSON) {
    super.init(title, data);

    this.chartType = 'bar';
    this.chartLabels = data['chartLabels'];
    this.chartData = data['chartData'];
    this.chartOptions = {
      responsive: true,
      legend: {
        display: false
      }
    };
  }

}
