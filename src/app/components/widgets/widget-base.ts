import {EventEmitter, Output} from '@angular/core';

export class WidgetBase {

  title: string;
  data: JSON;

  chartType: string;
  chartLabels: Array<string>;
  chartData: Array<Object>;
  chartOptions: Object;

  /**
   *
   * @type {EventEmitter<any>}
   */
  @Output()
  remove: EventEmitter<string> = new EventEmitter();

  /**
   *
   */
  constructor() {
  }

  /**
   *
   * @param {string} title
   * @param {JSON} data
   */
  init(title: string, data: JSON) {
    this.title = title;
    this.data = data;
  }

  /**
   *
   */
  onRemove() {
    this.remove.emit();
  }

}
