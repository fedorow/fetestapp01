import {Component} from '@angular/core';
import {WidgetBase} from '../widget-base';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-countries-widget',
  templateUrl: './countries-widget.component.html',
  styleUrls: ['./countries-widget.component.less']
})
export class CountriesWidgetComponent extends WidgetBase {

  /**
   *
   */
  public dataSource: MatTableDataSource<Object>;

  /**
   *
   * @type {string[]}
   */
  public columns = ['colName', 'colAuctions', 'colRequests', 'colBids', 'colViews', 'colClicks', 'colCTR', 'colCPC'];

  /**
   *
   */
  constructor() {
    super();
  }

  /**
   *
   * @param {string} title
   * @param {JSON} data
   */
  init(title: string, data: JSON) {
    super.init(title, data);

    this.dataSource = new MatTableDataSource(data['chartData']);
  }

}
