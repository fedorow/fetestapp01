export class Point {

  /**
   *
   */
  x: number;

  /**
   *
   */
  y: number;

  /**
   *
   * @param {number} x
   * @param {number} y
   */
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

}
