import {Type} from '@angular/core';
import {WidgetBase} from '../components/widgets/widget-base';

export class WidgetModel {

  /**
   * Content's type of widget
   */
  component: Type<WidgetBase>;

  /**
   * Title of Widget
   */
  title: string;

  /**
   * Data for widget
   */
  data: JSON;

  /**
   *
   * @param {Type<WidgetBase>} component
   * @param {string} title
   * @param {JSON} data
   */
  constructor(component: Type<WidgetBase>, title: string, data: JSON) {
    this.component = component;
    this.title = title;
    this.data = data;
  }
}
