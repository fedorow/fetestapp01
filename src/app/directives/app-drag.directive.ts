import {Directive, ElementRef, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {Point} from '../models/Point';

@Directive({
  selector: '[appDrag]'
})
export class AppDragDirective implements OnInit, OnDestroy {

  private static zIndex = 100;

  /**
   * Target of dragging
   */
  @Input()
  draggableTarget: any;

  /**
   *
   * @type {Point}
   */
  mousePosition = new Point();

  /**
   *
   * @param {ElementRef} elementRef
   * @param {Renderer2} renderer
   */
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }

  /**
   *
   */
  ngOnInit(): void {
    if (this.draggableTarget) {
      this.renderer.setStyle(this.elementRef.nativeElement, 'cursor', 'move');
      this.renderer.setAttribute(this.elementRef.nativeElement, 'draggable', 'true');

      this.elementRef.nativeElement.ondragstart = e => this.onDragStart(e);
      this.elementRef.nativeElement.ondrag = e => this.onDrag(e);
      this.elementRef.nativeElement.ondragend = e => this.onDrag(e);

      this.draggableTarget.onmousedown = e => this.onMouseDown(e);
      this.bringToFront();
    }
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.elementRef.nativeElement.ondragstart = null;
    this.elementRef.nativeElement.ondrag = null;
    this.elementRef.nativeElement.ondragend = null;

    this.draggableTarget.onmousedown = null;
  }

  /**
   * DragStart handler
   * @param e
   */
  onDragStart(e) {
    this.mousePosition.x = e.clientX - this.draggableTarget.offsetLeft;
    this.mousePosition.y = e.clientY - this.draggableTarget.offsetTop;

    const img = document.createElement('feedbackImage');
    e.dataTransfer.setDragImage(img, 0, 0);
    // e.dataTransfer.setData('text/plain', null);
  }

  /**
   * Drag handler
   * @param e
   */
  onDrag(e) {
    this.renderer.setStyle(this.draggableTarget, 'left', (e.clientX - this.mousePosition.x) + 'px');
    this.renderer.setStyle(this.draggableTarget, 'top', (e.clientY - this.mousePosition.y) + 'px');
  }

  /**
   * MouseDown handler
   * @param e
   */
  onMouseDown(e) {
    if (e.which === 1) {
      this.bringToFront();
    }
  }

  /**
   * Bring active widget to front of its container
   */
  bringToFront() {
    this.renderer.setStyle(this.draggableTarget, 'z-index', AppDragDirective.zIndex.toString());
    AppDragDirective.zIndex++;
  }

}
