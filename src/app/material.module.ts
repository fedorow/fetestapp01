import {NgModule} from '@angular/core';

import {MatButtonModule, MatIconModule, MatMenuModule, MatSelectModule, MatTableModule, MatToolbarModule} from '@angular/material';

const MAT_MODULES = [
  MatIconModule,
  MatSelectModule,
  MatButtonModule,
  MatToolbarModule,
  MatMenuModule,
  MatTableModule
];

@NgModule({
  imports: MAT_MODULES,
  exports: MAT_MODULES
})
export class MaterialModule {
}
