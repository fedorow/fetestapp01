import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from './material.module';
import {ChartsModule} from 'ng2-charts';

import {AppComponent} from './components/app-component/app.component';
import {AuctionsWidgetComponent} from './components/widgets/auctions-widget/auctions-widget.component';
import {DevicesWidgetComponent} from './components/widgets/devices-widget/devices-widget.component';
import {DemographicsWidgetComponent} from './components/widgets/demographics-widget/demographics-widget.component';
import {CountriesWidgetComponent} from './components/widgets/countries-widget/countries-widget.component';

import {AppDragDirective} from './directives/app-drag.directive';

@NgModule({
  declarations: [
    AppComponent,
    AuctionsWidgetComponent,
    DevicesWidgetComponent,
    DemographicsWidgetComponent,
    CountriesWidgetComponent,
    AppDragDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    MaterialModule,
    ChartsModule
  ],
  providers: [],
  entryComponents: [
    AuctionsWidgetComponent,
    DevicesWidgetComponent,
    DemographicsWidgetComponent,
    CountriesWidgetComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
