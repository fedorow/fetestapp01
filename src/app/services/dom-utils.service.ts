import {ComponentFactoryResolver, ComponentRef, Injectable, ViewContainerRef} from '@angular/core';
import {Type} from '@angular/core/src/type';

@Injectable()
export class DomUtilsService {

  /**
   *
   * @param {ComponentFactoryResolver} factoryResolver
   */
  constructor(private factoryResolver: ComponentFactoryResolver) {
  }

  /**
   * Add component of type T to container
   * @param {ViewContainerRef} container
   * @param {Type<T>} type
   * @returns {ComponentRef<T>}
   */
  addComponent<T>(container: ViewContainerRef, type: Type<T>): ComponentRef<T> {
    const factory = this.factoryResolver.resolveComponentFactory(type);
    const component = factory.create(container.parentInjector);

    container.insert(component.hostView);

    return component;
  }

}
