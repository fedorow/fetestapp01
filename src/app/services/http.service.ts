import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class HttpService {

  private static DATA_FILE = './assets/data.json';

  /**
   *
   * @param {HttpClient} http
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Get RAW data from external source
   * @returns {Observable<any>}
   */
  public loadData(): Observable<any> {
    return this.http.get(HttpService.DATA_FILE);
  }

}
